Type:    Console Application

Stores a sample of 5 contacts (first and last name) and allows the user to search for a person

Requirements: 

·       Display partial matches

·       Display all full matches

Purpose: Learn C#: Collections and Generics

Weight: Basic