﻿using System;
using System.Collections.Generic;

namespace YellowPages
{
    public class Contact
    {
        public string FirstName
        {
            get; private set;
        }

        public string LastName
        {
            get; private set;
        }

        public string FullName
        {
            get
            {
                return ToString();
            }
        }

        public string Company
        {
            get; private set;
        }

        public Contact(string FullName)
        {
            if (FullName.Contains(" "))
            {
                string[] name = FullName.Split(' ');

                this.FirstName = name[0];
                this.LastName = name[1];
                this.Company = string.Empty;

                if (name.Length > 2 && name[2].Length != 0)
                {
                    this.Company = name[2];
                }
            }
            else
            {
                throw new Exception("Invalid full name given.");
            }
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName} {(Company != string.Empty ? $"({Company})" : "")}";
        }
    }

    class MainClass
    {
        static List<Contact> contacts;

        public static Contact CreateContact(string FullName)
        {
            return new Contact(FullName);
        }

        public static void Main(string[] args)
        {
            string[] contactNames = new string[] {
                "John Doe",
                "John Appleseed",
                "Nikola Tesla",
                "Bill Gates Microsoft",
                "Steve Jobs Apple",
                "Steve Wozniak Apple",
                "Elon Musk Telsa/SpaceX",
                "Jack Ma Alibaba",
                "Tim Cook Apple",
                "Jack Dorsey Twitter",
                "Larry Page Google",
                "Larry Ellison Oracle",
                "Jeff Bezos Amazon",
                "Michael Dell Dell",
                "Mark Zuckerberg Facebook",
            };

            Setup(contactNames);

            bool appState = true;
            while (appState)
            {
                PrintContactList();
                appState = UserInput();
            }
        }

        public static bool UserInput()
        {
            Console.Write("Please enter a name to search: ");
            string input = Console.ReadLine();
            bool state = true;

            switch (input)
            {
                case "exit":
                    state = false;
                    break;
                default:
                    Search(input);
                    break;
            }

            return state;
        }

        /*
         * Sets up the application by creating a new Contact list and then
         * adding all names to the contact list.
         */
        public static void Setup(string[] nameList)
        {
            contacts = new List<Contact>();

            foreach (string contact in nameList)
            {
                contacts.Add(CreateContact(contact));
            }
        }
        /*
         * The method that handles searching the contact list and dictating
         * what a match is (partial or full)
         */
        public static void Search(string str)
        {
            List<string> full = new List<string>();
            List<string> partial = new List<string>();

            Console.WriteLine($"\nSearching contact list for '{str}': ");
            foreach (Contact contact in contacts)
            {
                // Check if the searched string partially resembles the first name
                if (contact.FirstName.Contains(str) || contact.FirstName.StartsWith(str) && !partial.Contains(contact.FullName))
                    partial.Add(contact.FullName);

                // Check if the searched string partially resembles the last name
                if (contact.LastName.Contains(str) || contact.LastName.StartsWith(str) && !partial.Contains(contact.FullName))
                    partial.Add(contact.FullName);

                // Check if the searched string partially resembles the full name
                if (contact.FullName.Contains(str) && !partial.Contains(contact.FullName))
                    partial.Add(contact.FullName);

                // Check if the searched string fully resembles the full name
                if (contact.FullName.Equals(str) && !full.Contains(str))
                    full.Add(contact.FullName);

                // Check if the searched string fully resembles the first name
                if (contact.FirstName.Equals(str) && !full.Contains(str))
                    full.Add(contact.FullName);

                // Check if the searched string fully resembles the last name
                if (contact.LastName.Equals(str) && !full.Contains(str))
                    full.Add(contact.FullName);

                // If a full match is contained in the list of partial matches, remove the partial match
                if (partial.Contains(contact.FullName) && full.Contains(contact.FullName))
                {
                    partial.Remove(contact.FullName);
                }
            }

            SetConsoleColor(ConsoleColor.DarkYellow);
            // Print all the full matches
            foreach (string match in full)
            {
                Console.WriteLine($"    {match}");
            }

            // Print all the partial matches
            foreach (string match in partial)
            {
                Console.WriteLine($"    {match} - Partial");
            }
            Console.WriteLine($"Got {partial.Count} partial matches and {full.Count} full matches.\n");
            SetConsoleColor(null);
        }

        /*
         * Prints out a welcome message and all the contacts in the list.
         */
        static void PrintContactList()
        {
            Console.WriteLine("Welcome to the Yellow Pages!");
            Console.WriteLine("In a moment you will be able to search for your contacts");
            Console.WriteLine("If you wish to exit at any time, please type 'exit' in the search field.");

            Console.WriteLine($"Listing all {contacts.Count} contacts:");
            foreach (Contact contact in contacts)
            {
                Console.WriteLine($"    - {contact}");
            }
        }

        /*
         * Sets the color of text shown in the console window.
         * If no color is given, then by default the console
         * text color will be set to white.
         */
        static void SetConsoleColor(ConsoleColor? color)
        {
            if (color.HasValue)
            {
                Console.ForegroundColor = color.Value;
                return;
            }

            Console.ForegroundColor = ConsoleColor.White;
        }

    }
}
